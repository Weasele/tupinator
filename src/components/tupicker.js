import React, {useState, useEffect, useRef} from 'react';
import { motion } from "framer-motion"
import styled from 'styled-components'
import Moment from 'react-moment';
import Nyan from "../images/nyan.gif"




const Tupicker = () => {
    const [list, setList] = useState([]); 
    const [newItem, setNewItem] = useState({key: "", start: "", end: ""});
    const [selected, setSelected] = useState(0);
    const [initialLoad, setInitialLoad] = useState(false);
    const [isReady, setIsReady] = useState(false);
    const [clearing, setClearing] = useState(true)
    const [tupinating, setIsTupinating] = useState(false)
    const [nyanNyan, setNyanNyan] = useState(false)
    const audioRef = useRef();

   
  

    const SetStart = (e) => {
        const newValue = {start: e.target.value}
        setNewItem(old => {
            return({...old, ...newValue})
        });
    }

    const SetEnd = (e) =>  {
        const newValue = {end: e.target.value}
        setNewItem(old => {
            return({...old, ...newValue})
        });
    }

    const Add = () => {
        if(newItem.start && newItem.end != "Invalid date") {
            const genKey = Math.floor(Math.random() * Math.floor(999999));
            setList(old => [...old, {key: genKey, start: newItem.start, end: newItem.end}])
        }
    }

    const Delete = (key) => {
         const newList = list.filter(item => item.key != selected)
         setList(newList);
         setSelected(false)

    }

    const SetSelect = (key) => {
        setSelected(key);
    }


    const Check = () => {
        alert(newItem.start > newItem.end)
    }

    const StartMusic = () => {
        audioRef.current.play()
    }

    const Tupinate = () => {
        if( list.length > 0) {
            setIsTupinating(true)
            setTimeout(()=>{
                StartMusic()
                setNyanNyan(true)
            }, 15000)
        }
    }
    // const EraseText = () => {
    //     var ref;
    //     if(typedText.length > 0) {
    //         var rand = Math.floor(Math.random() * Math.floor(300));
    //         ref = setTimeout(()=>{
    //             setTypedText(typedText.slice(0, -1))
    //         },rand)
    //     } else {
    //         clearInterval(ref)
    //         setClearing(false)
    //     }
    // }



    

    useEffect(()=>{
        setInitialLoad(true) //initial load completed
        if(!initialLoad) {}
    }, [])

    const vars = {
        init: {
            height: ".2%",
            width: "0vw"
        },
        foldout: {
            height: [".2%", ".2%", ".2%", "100%"], 
            width: ["0vw", "75vw", "75vw", "75vw"],
        },
    }

    return(
        <TupContainer onClick={()=>{setSelected(false)}}>
            <FormContainer
                tupinating={tupinating} 
                variants={vars}
                initial={"init"}
                animate={!initialLoad && "foldout"}
                transition={{ duration: 3, delay: 1}}
            >
                <TupTitle> 
                    Please pick the dates of your <TupinationText animate={{ backgroundSize: ["0%" , "80%", "50%", "100%", "20%"]  }} transition={{ duration: 10, delay: 3, repeatType: "reverse", repeat: Infinity}}> TUPINATION</TupinationText>.
                    <Blink 
                        animate={{ opacity: [0, 1] }}
                        transition={{ repeat: Infinity, repeatDelay: .2,  repeatType: "reverse", duration: .5 }}
                    />
                    
                </TupTitle>
                <Content              
                    animate={ !initialLoad && { opacity: [0, 1] }}
                    initial={{ opacity: 0 }}
                    transition={{ duration: 5, delay: 3}}
                >
                    <div>
                        <input type="date" onChange={SetStart}/>
                        <input type="date" onChange={SetEnd}/>
                    </div>
                    <TupList onClick={(e)=> { e.stopPropagation()}}>
                        <TupListHeading><div>Start</div>|<div>End</div></TupListHeading>
                        {list.map((item) => {
                            return(<TupListItem  onClick={(e)=>{ e.stopPropagation(); SetSelect(item.key)}} isSelected={item.key == selected}><Moment format="MM/DD/YYYY">{item.start}</Moment><Moment format="MM/DD/YYYY">{item.end}</Moment></TupListItem>)
                        })}
                    </TupList>
                    <Buttons>
                        {!selected && <TupinateButton onClick={Add}>Add</TupinateButton>}
                        {selected && <TupinateButton onClick={Delete}>Delete</TupinateButton>}
                        <TupinaterButton onClick={Tupinate} isReady={list.length > 0} disabled={!list.length > 0} animate={{ backgroundSize: ["100%" , "120%", "160%", "200%"]  }} transition={{ duration: 3, repeatType: "reverse", repeat: Infinity}}>TUPINATE</TupinaterButton>
                    </Buttons>
                </Content>
            </FormContainer>
             <NyanContainer nyanNyan={nyanNyan}>
              <TheNyan src={Nyan}/>
                <audio ref={audioRef}>
                <source src={process.env.PUBLIC_URL + 'nyan.mp3'} type="audio/ogg"/>
                <source src={process.env.PUBLIC_URL + 'nyan.ogg'} type="audio/mpeg"/>
                Your browser does not support the audio element.
                </audio>
                </NyanContainer>
                <WarningContainer tupinating={tupinating} animate={{ opacity: [0, 1] }}  transition={{ duration: 3, delay: 2}}>
                    <Warning animate={{ opacity: [0, 1, 1, 1, 0] }}  transition={{ duration: 5, delay: 2}}>Remember... </Warning>
                    <Warning animate={{ opacity: [0, 1, 1, 1, 0] }} transition={{ duration: 7, delay: 8}}>You asked for this...</Warning>
                </WarningContainer>
        </TupContainer>
    )
}


const Warning = styled(motion.div)``
const WarningContainer = styled(motion.div)`
    position: absolute;
    height: 100vh;
    width: 100vw;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: white;
    font-weight: bold;
    font-size: 5em;
    z-index: 2;
    background: black;
    display: ${props => props.tupinating ? "" : 'none'}

`

const CountDown = styled(motion.div)`
    color: white;
    font-size: 5em;
    
`

const NyanContainer = styled.div`
    position: fixed;
    display: ${props => !props.nyanNyan ? 'none': ''};
    z-index: 99;
`
const TheNyan = styled.img``

const Blink = styled(motion.span)`
    background: white;
    display: inline-block;
    height: 100%;
    width: 2px;
`
const Content = styled(motion.div)`
    display: grid;
    grid-gap: 2%;
    justify-items: center;
    align-content: center; 
    opacity: 0;
    
    
`

const TupinationText = styled(motion.strong)`
    background-color: #f3ec78;
    background-image: linear-gradient(90deg, rgba(242,154,255,1) 0%, rgba(255,175,0,1) 0%, rgba(255,85,222,1) 100%);
    background-size: 100%;
    -webkit-background-clip: text;
    -moz-background-clip: text;
    -webkit-text-fill-color: transparent; 
    -moz-text-fill-color: transparent;
`

const Buttons = styled.div`
    display: flex;
    flex-direction: column;
`

const TupinateButton = styled.button`
    width: 300px;
    height: 50px;
    font-weight: bold;
    background: white;
    font-size: 1.5em;    
`


const TupinaterButton = styled(motion.button)`
    width: 300px;
    height: 50px;
    font-weight: bold;
    font-size: 1.5em;  
    background-color: ${props => !props.isReady ? "white" : `#f3ec78`};
    background-image: ${props => !props.isReady ? "white" : `linear-gradient(90deg, rgba(242,154,255,1) 0%, rgba(255,175,0,1) 0%, rgba(255,85,222,1) 100%)`};
    background-size: 100%;
    
`

const TupTitle = styled.div`
    color: white;
    font-size: 2em;
    margin: 5%;
    
`

const TupListItem = styled.div`
    margin: 2%;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    cursor: pointer;
    color: ${props => props.isSelected ? "red" : "black"};
    border: ${props => props.isSelected ? "solid red 2px" : ""};
`

const TupListHeading = styled(TupListItem)`
    border: solid 1px;
    cursor: default;
`

const TupList = styled.div`
    background: white;
    height: 300px;
    width: 300px;
    display: flex;
    flex-direction: column;
    overflow: auto;
      font-family: 'Ubuntu Mono', monospace; 
    

`

const FormContainer = styled(motion.div)`
    border: solid white;
    border-left: none;
    border-right: none;
    display: grid;
    grid-template-rows: 10%  80%;
    position: relative;
    height: 100%;
    overflow: hidden;
    display: ${props => props.tupinating ? 'none' : '' }
    
`

const TupContainer = styled(motion.div)`
    position: absolute;
    height: 100%;
    width: 100%;
    display: flex;
    height: 100%;
    justify-content: center;
    align-items: center;
    
`


export default Tupicker;