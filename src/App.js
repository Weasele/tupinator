import logo from './logo.svg';
import './App.css';
import Tupicker from './components/tupicker'

function App() {
  return (
    <div className="App">
      <Tupicker/>
    </div>
  );
}

export default App;
